Imports System.ServiceProcess
Imports System.Web.HttpUtility
Imports System.Net
Imports System.Web.Mail
Imports System.Text
Imports System.Xml
Imports System.IO
Imports ntb_FuncLib
Imports System.Configuration.ConfigurationSettings

Public Class ValgQueryService
    Inherits System.ServiceProcess.ServiceBase

#Region " Component Designer generated code "

    Public Sub New()
        MyBase.New()

        ' This call is required by the Component Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call

    End Sub

    'UserService overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    ' The main entry point for the process
    <MTAThread()> _
    Shared Sub Main()
        Dim ServicesToRun() As System.ServiceProcess.ServiceBase

        ' More than one NT Service may run within the same process. To add
        ' another service to this process, change the following line to
        ' create a second service object. For example,
        '
        '   ServicesToRun = New System.ServiceProcess.ServiceBase () {New Service1, New MySecondUserService}
        '
        ServicesToRun = New System.ServiceProcess.ServiceBase() {New ValgQueryService()}

        System.ServiceProcess.ServiceBase.Run(ServicesToRun)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    ' NOTE: The following procedure is required by the Component Designer
    ' It can be modified using the Component Designer.  
    ' Do not modify it using the code editor.
    Friend WithEvents pollTimer As System.Timers.Timer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.pollTimer = New System.Timers.Timer()
        CType(Me.pollTimer, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'pollTimer
        '
        Me.pollTimer.Interval = 15000
        '
        'ValgQueryService
        '
        Me.CanShutdown = True
        Me.ServiceName = "ValgQuery"
        CType(Me.pollTimer, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

#Region " Service wide vars "

    'Folders
    Dim logFiles As String
    Dim queryLog As String
    Dim xmlOut As String

    'Logfiles
    Dim systemLog As StreamWriter
    Dim errorLog As StreamWriter

    'Query
    Dim server As String
    Dim method As String
    Dim interval As Integer
    Dim timeout As Integer

    Dim index As Integer 'Last R06 report id

    'Warnings
    Dim warnEmail As Boolean
    Dim emailRcpt As String
    Dim SMTPServer As String

    Dim warnSMS As Boolean
    Dim SMSRcpt As String
    Dim SMSPath As String

    'Status
    Dim online As Boolean

    'Report fetch controll
    Dim getFLands = False
    Dim getKLands = False
    Dim getF03 = False
    Dim getK03 = False
    Dim getK09 = False

    'Other
    Dim fetchedFylker As ArrayList
    Dim fetchedKommuner As ArrayList
    Dim XMLDoc As XmlDocument 'Reusable XML doc

#End Region

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        Init()

        systemLog.WriteLine(Now & " : ValgQuery Service: Starting timer")

        pollTimer.Interval = interval
        pollTimer.Enabled = True
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        pollTimer.Enabled = False

        'Log event
        systemLog.WriteLine(Now & " : -- ValgQuery Service stopped --")

        systemLog.Close()
    End Sub

    Protected Overrides Sub OnShutdown()
        ' On server shutdown
        OnStop()
    End Sub

    Private Sub pollTimer_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles pollTimer.Elapsed

        pollTimer.Enabled = False

        'Perform R06 query at each timer tick
        PerformR06Query()

        pollTimer.Enabled = True

    End Sub

#Region " Service action functions "

    'Load settings and initiate server
    Private Sub Init()

        'Init vars
        XMLDoc = New XmlDocument()
        fetchedFylker = New ArrayList
        fetchedKommuner = New ArrayList

        'Load settings
        logFiles = AppSettings("logFileFolder")
        queryLog = AppSettings("queryLogFolder")
        xmlOut = AppSettings("xmlOutFolder")

        'create folders:
        LogFile.MakePath(logFiles)
        LogFile.MakePath(queryLog)
        LogFile.MakePath(xmlOut)

        'Set up logging
        systemLog = New StreamWriter(logFiles & "system-log.txt", True, Encoding.GetEncoding("iso-8859-1"))
        systemLog.AutoFlush = True

        'Log event
        systemLog.WriteLine(Now & " : -- ValgQuery Service: Logging started --")

        'Query settings
        server = AppSettings("queryServer")
        method = AppSettings("queryMethod")
        interval = AppSettings("queryInterval") * 1000
        timeout = AppSettings("queryTimeout") * 1000

        'Warning settings
        If (AppSettings("warnEmail") = "True") Then warnEmail = True Else warnEmail = False
        emailRcpt = AppSettings("emailRcpt")
        SMTPServer = AppSettings("SMTPServer")

        If (AppSettings("warnSMS") = "True") Then warnSMS = True Else warnSMS = False
        SMSRcpt = AppSettings("SMSRcpt")
        SMSPath = AppSettings("SMSPath")

        'Initial status
        online = True

        'Get the report index, l�penummer
        index = 0
        If File.Exists(queryLog & "lastnode.xml") Then

            Dim ok As Boolean = True
            Try
                XMLDoc.Load(queryLog & "lastnode.xml")
            Catch e As Exception
                ok = False
                systemLog.WriteLine(Now & " : Failed to load report index (L�peNr)")
                LogError(e)
            End Try

            If ok Then
                'Find the list nodes
                Dim idx As Integer
                Dim node As XmlNode
                Dim nodelist As XmlNodeList = XMLDoc.SelectNodes("/liste")

                'Loop trough, find 'LopeNr'
                For Each node In nodelist
                    idx = node.SelectSingleNode("data[@navn='RapportLopeNr']").InnerText
                    If idx > index Then index = idx
                Next

                systemLog.WriteLine(Now & " : Report index (L�peNr) loaded from disk: " & index)
            End If
        End If
    End Sub

    'Performing the timed R06 query
    Private Function PerformR06Query()

        'Vars
        Dim query As String
        Dim R06Response As String
        Dim response As String
        Dim nodeList As XmlNodeList
        Dim node As XmlNode

        Dim ok As Boolean = True
        Dim continue As Boolean = True


        'Reset fylkeliste
        fetchedFylker.Clear()

        'reset kommuneliste
        fetchedKommuner.Clear()

        'Log event
        systemLog.WriteLine(Now & " : Performing R06 query.")

        'Build R06 query
        If index = 0 Then
            query = CreateXMLQuery("R06", , , , , , "20030101", "00:00:00")
        Else
            query = CreateXMLQuery("R06", index)
        End If

        'DEBUG:
        'DumpToFile(query, queryLog, "R06-query-debug.xml")

        'Fetch the XML data
        ok = True
        R06Response = DoQuery(query)

        'DEBUG:
        'DumpToFile(R06Response, queryLog, "R06-debug.xml")

        'Try to load the XML
        Try
            XMLDoc.LoadXml(R06Response)
        Catch e As Exception
            ok = False
            continue = False
            'Log error
            systemLog.WriteLine(Now & " : Bad R06 response.")
            LogError(e)
        End Try

        'Continue actions if we have a valid xml
        If ok Then

            'Find the list nodes
            nodeList = XMLDoc.SelectNodes("/respons/rapport/tabell/liste")

            'Log event 
            systemLog.WriteLine(Now & " : R06 response ok, " & nodeList.Count & " updates.")

            'If nodeList.Count > 0 Then Threading.Thread.Sleep(5000)

            'Loop trough, find 'LopeNr' and handle them individually
            'Updating kommune/fylke for each element individually
            Dim i As Integer
            For i = nodeList.Count - 1 To 0 Step -1

                node = nodeList(i)

                'LopeNr - Index
                Dim idx As Integer = node.SelectSingleNode("data[@navn='RapportLopeNr']").InnerText
                Dim valg As String = node.SelectSingleNode("data[@navn='Valgtype']").InnerText
                Dim kretsNr As String = node.SelectSingleNode("data[@navn='KretsNr']").InnerText
                Dim kommNr As String = node.SelectSingleNode("data[@navn='KommNr']").InnerText

                If valg = "F" Then
                    getFLands = True
                    If kretsNr <> "" Then getF03 = True
                ElseIf valg = "K" Then
                    getKLands = True
                    If kretsNr <> "" Then getK03 = True
                    If kommNr = 301 Then getK09 = True 'Oslo = 0301
                End If

                'Fetch other reports based on current entry
                ok = HandleUpdateEntry(node)
                If Not ok Then
                    continue = False
                    Exit For
                ElseIf idx > index Then
                    index = idx
                End If
            Next

            'Dump to disk if we had updates, these are not necessearily all updated !!
            If nodeList.Count > 0 Then
                'Current
                DumpToFile(R06Response, queryLog, "R06.xml")
                'Archive
                DumpToFile(R06Response, queryLog & "R06\", "R06-" & Now.ToString("yyyyMMdd_HHmmss") & ".xml")
            Else
                continue = False
            End If
        End If

        'End While

        ok = True

        'Landsoversikter and other general lists is delayed until end of loop

        'Kommunevalg
        If getKLands Then

            'Fetch the K05 report (landsoversikt)
            query = CreateXMLQuery("K05")
            response = DoQuery(query)

            'Try to load the XML
            Try
                XMLDoc.LoadXml(response)
            Catch e As XmlException
                ok = False

                'Log event
                systemLog.WriteLine(Now & " : Bad K05 response.")
                LogError(e)
            End Try

            'Store to disk
            If ok Then
                'Log event 
                systemLog.WriteLine(Now & " : K05 fetched. (Landsoversikt, kommunevalg)")

                DumpToFile(response, xmlOut, "K05.xml")
                DumpToFile(response, queryLog & "Kommune\K05\", "K05" & "-" & Now.ToString("yyyyMMdd_HHmmss") & ".xml")

            End If

            'Fetch framm�te for kommunevalget 10 h�y/10 lav - K07
            If ok Then
                query = CreateXMLQuery("K07")
                response = DoQuery(query)

                'Try to load the XML
                Try
                    XMLDoc.LoadXml(response)
                Catch e As XmlException
                    ok = False

                    'Log event
                    systemLog.WriteLine(Now & " : Bad K07 response.")
                    LogError(e)
                End Try

                'Store to disk
                If ok Then
                    'Log event 
                    systemLog.WriteLine(Now & " : K07 fetched. (Oppm�te, h�y/lav - kommunevalg)")

                    DumpToFile(response, xmlOut, "K07.xml")
                    DumpToFile(response, queryLog & "Kommune\K07\", "K07" & "-" & Now.ToString("yyyyMMdd_HHmmss") & ".xml")
                End If
            End If


            'Fetch opptellingsoversikt-kommunevalg, K01 for alle fylker
            If ok Then
                query = CreateXMLQuery("K01", , "Alle")
                response = DoQuery(query)

                'Try to load the XML
                Try
                    XMLDoc.LoadXml(response)
                Catch e As XmlException
                    ok = False

                    'Log event
                    systemLog.WriteLine(Now & " : Bad K01 response.")
                    LogError(e)
                End Try

                'Store to disk
                If ok Then

                    'Log event 
                    systemLog.WriteLine(Now & " : K01 fetched. (Opptellingsstatus, kommunevalg: Alle fylker)")

                    DumpToFile(response, xmlOut, "K01.xml")
                    DumpToFile(response, queryLog & "Kommune\K01\", "K01" & "-" & Now.ToString("yyyyMMdd_HHmmss") & ".xml")
                    getKLands = False
                End If

            End If
        End If

        'Get the complete merged kretsoversikt
        If ok And getK03 Then
            query = CreateXMLQuery("K03", , "Alle")
            response = DoQuery(query)

            'Try to load the XML
            Try
                XMLDoc.LoadXml(response)
            Catch e As XmlException
                ok = False
                'Log error
                systemLog.WriteLine(Now & " : Bad K03 response.")
                LogError(e)
            End Try

            'Handle returned data
            If ok Then
                'Log event
                systemLog.WriteLine(Now & " : K03 fetched. (Alle kretser - Kommunevalg)")

                DumpToFile(response, xmlOut, "K13.xml")
                DumpToFile(response, queryLog & "Kommune\K03\", "K13-" & Now.ToString("yyyyMMdd_HHmmss") & ".xml")

                getK03 = False
            End If
        End If

        'Bystyre/bydels reports for oslo
        If ok And getK09 Then

            'Bystyre - K09
            query = CreateXMLQuery("K09")
            response = DoQuery(query)

            'Try to load the XML
            Try
                XMLDoc.LoadXml(response)
            Catch e As XmlException
                ok = False
                'Log error
                systemLog.WriteLine(Now & " : Bad K09 response.")
                LogError(e)
            End Try

            'Store to disk
            If ok Then
                Dim n As XmlNode
                Dim rspNavn As String
                Dim rspOmrNr As String

                Try
                    n = XMLDoc.SelectSingleNode("/respons/rapport/data[@navn='KommNavn']")
                    rspNavn = n.InnerText
                    n = XMLDoc.SelectSingleNode("/respons/rapport/data[@navn='KommNr']")
                    rspOmrNr = n.InnerText
                Catch e As Exception
                    ok = False
                    'Log error
                    systemLog.WriteLine(Now & " : Bad K09 response.")
                    LogError(e)
                End Try

                If ok Then
                    'Log event
                    systemLog.WriteLine(Now & " : K09 fetched. (" & rspNavn & ", Bystyreoversikt)")

                    DumpToFile(response, xmlOut, "K09.xml")
                    DumpToFile(response, queryLog & "Kommune\K09\", "K09" & "-" & Now.ToString("yyyyMMdd_HHmmss") & ".xml")

                    'getK09 = False
                End If
            End If

            'Bydelsoversikt - K08
            query = CreateXMLQuery("K08", , "Alle")
            response = DoQuery(query)

            'Try to load the XML
            Try
                XMLDoc.LoadXml(response)
            Catch e As XmlException
                ok = False
                'Log error
                systemLog.WriteLine(Now & " : Bad K08 response.")
                LogError(e)
            End Try

            'Store to disk
            If ok Then
                Dim n As XmlNode
                Dim rspNavn As String

                Try
                    n = XMLDoc.SelectSingleNode("/respons/rapport/data[@navn='KommNavn']")
                    rspNavn = n.InnerText
                Catch e As Exception
                    ok = False
                    'Log error
                    systemLog.WriteLine(Now & " : Bad K08 response.")
                    LogError(e)
                End Try

                If ok Then
                    'Log event
                    systemLog.WriteLine(Now & " : K08 fetched. (" & rspNavn & ", Bydelsoversikt)")

                    DumpToFile(response, xmlOut, "K28.xml")
                    DumpToFile(response, queryLog & "Kommune\K08\", "K28" & "-" & Now.ToString("yyyyMMdd_HHmmss") & ".xml")

                    getK09 = False
                End If

            End If
        End If

        'Fylkestingsvalg
        If ok And getFLands Then

            'Fetch the F05 report (landsoversikt)
            query = CreateXMLQuery("F05")
            response = DoQuery(query)

            'Try to load the XML
            Try
                XMLDoc.LoadXml(response)
            Catch e As XmlException
                ok = False

                'Log event
                systemLog.WriteLine(Now & " : Bad F05 response.")
                LogError(e)
            End Try

            'Store to disk
            If ok Then
                'Log event 
                systemLog.WriteLine(Now & " : F05 fetched. (Landsoversikt, fylkestingsvalg)")

                DumpToFile(response, xmlOut, "F05.xml")
                DumpToFile(response, queryLog & "Fylke\F05\", "F05" & "-" & Now.ToString("yyyyMMdd_HHmmss") & ".xml")
            End If

            'Fetch opptellingsoversikt-fylkestingsvalg, F01 for alle fylker
            If ok Then
                query = CreateXMLQuery("F01", , "Alle")
                response = DoQuery(query)

                'Try to load the XML
                Try
                    XMLDoc.LoadXml(response)
                Catch e As XmlException
                    ok = False

                    'Log event
                    systemLog.WriteLine(Now & " : Bad F01 response.")
                    LogError(e)
                End Try

                'Store to disk
                If ok Then
                    'Log event 
                    systemLog.WriteLine(Now & " : F01 fetched. (Opptellingsstatus, fylkestingsvalg: Alle fylker)")

                    DumpToFile(response, xmlOut, "F01.xml")
                    DumpToFile(response, queryLog & "Fylke\F01\", "F01" & "-" & Now.ToString("yyyyMMdd_HHmmss") & ".xml")
                End If
            End If

            'Fetch the F07 report (landsoversikt, fylkesvis)
            If ok Then
                query = CreateXMLQuery("F07")
                response = DoQuery(query)

                'Try to load the XML
                Try
                    XMLDoc.LoadXml(response)
                Catch e As XmlException
                    ok = False

                    'Log event
                    systemLog.WriteLine(Now & " : Bad F07 response.")
                    LogError(e)
                End Try

                'Store to disk
                If ok Then
                    'Log event 
                    systemLog.WriteLine(Now & " : F07 fetched. (Fylkesvis landsoversikt, fylkestingsvalg)")

                    DumpToFile(response, xmlOut, "F07.xml")
                    DumpToFile(response, queryLog & "Fylke\F07\", "F07" & "-" & Now.ToString("yyyyMMdd_HHmmss") & ".xml")
                    getFLands = False
                End If
            End If
        End If

        'Get the complete merged kretsoversikt
        If ok And getF03 Then
            query = CreateXMLQuery("F03", , "Alle")
            response = DoQuery(query)

            'Try to load the XML
            Try
                XMLDoc.LoadXml(response)
            Catch e As XmlException
                ok = False
                'Log error
                systemLog.WriteLine(Now & " : Bad F03 response.")
                LogError(e)
            End Try

            'Handle returned data
            If ok Then

                'Log event
                systemLog.WriteLine(Now & " : F03 fetched. (Alle kretser - Fylkestingsvalg)")

                DumpToFile(response, xmlOut, "F13.xml")
                DumpToFile(response, queryLog & "Fylke\F03\", "F13-" & Now.ToString("yyyyMMdd_HHmmss") & ".xml")

                getF03 = False
            End If

        End If

    End Function

    ' Takes a list node from the R06 report and fetches/updates other reports based on the new info
    Private Function HandleUpdateEntry(ByVal node As XmlNode) As Boolean

        Dim valg As String = node.SelectSingleNode("data[@navn='Valgtype']").InnerText
        Dim fylkeNr As Integer = node.SelectSingleNode("data[@navn='FylkeNr']").InnerText
        Dim kommNr As Integer = node.SelectSingleNode("data[@navn='KommNr']").InnerText
        Dim kretsNr As String = node.SelectSingleNode("data[@navn='KretsNr']").InnerText
        Dim kommNavn As String = node.SelectSingleNode("data[@navn='KommNavn']").InnerText
        Dim kretsNavn As String = node.SelectSingleNode("data[@navn='KretsNavn']").InnerText

        'NTB Custom, adds status to the reports
        Dim statusInd As String = node.SelectSingleNode("data[@navn='StatusInd']").InnerText
        Dim strStatusElement As String = "<data navn=""StatusInd"">" & statusInd & "</data>"

        Dim n As XmlNode
        Dim rspNavn As String
        Dim rspOmrNr As String
        Dim rspKretsNr As String

        Dim tmp As String

        Dim ok As Boolean = True
        Dim nr As Integer
        Dim query As String
        Dim response As String

        'Switch on valg type - F/K
        If valg = "K" Then

            'Fetch the K02 report
            query = CreateXMLQuery("K02", , kommNr)
            response = DoQuery(query)

            'Try to load the XML
            Try
                XMLDoc.LoadXml(response)
            Catch e As XmlException
                ok = False
                'Log error
                systemLog.WriteLine(Now & " : Bad K02 response.") ' & vbCrLf & response
                LogError(e)
            End Try

            'Store to disk
            If ok Then
                Try
                    n = XMLDoc.SelectSingleNode("/respons/rapport/data[@navn='KommNavn']")
                    rspNavn = n.InnerText
                    n = XMLDoc.SelectSingleNode("/respons/rapport/data[@navn='KommNr']")
                    rspOmrNr = n.InnerText
                Catch e As Exception
                    ok = False

                    'Log error
                    systemLog.WriteLine(Now & " : Bad K02 response.")
                    LogError(e)
                End Try

                If ok Then
                    'Log event
                    systemLog.WriteLine(Now & " : K02, Kommunenr: " & rspOmrNr & " fetched. (" & rspNavn & ")")
                    fetchedKommuner.Add("K" & kommNr)

                    response = response.Replace("</rapportnavn>", "</rapportnavn>" & vbCrLf & strStatusElement)
                    DumpToFile(response, xmlOut, "K02-" & rspOmrNr & ".xml")
                    DumpToFile(response, queryLog & "Kommune\K02\", "K02-" & rspOmrNr & "-" & Now.ToString("yyyyMMdd_HHmmss") & ".xml")
                End If
            End If

            'Fetch the K04 report
            tmp = "K" & fylkeNr
            If ok And Not fetchedFylker.Contains(tmp) Then
                query = CreateXMLQuery("K04", , fylkeNr)
                response = DoQuery(query)

                'Try to load the XML
                Try
                    XMLDoc.LoadXml(response)
                Catch e As XmlException
                    ok = False
                    'Log error
                    systemLog.WriteLine(Now & " : Bad K04 response.")
                    LogError(e)
                End Try

                'Store to disk
                If ok Then
                    Try
                        n = XMLDoc.SelectSingleNode("/respons/rapport/data[@navn='FylkeNavn']")
                        rspNavn = n.InnerText
                        n = XMLDoc.SelectSingleNode("/respons/rapport/data[@navn='FylkeNr']")
                        rspOmrNr = n.InnerText
                    Catch e As Exception
                        ok = False

                        'Log error
                        systemLog.WriteLine(Now & " : Bad K04 response.")
                        LogError(e)
                    End Try

                    If ok Then
                        fetchedFylker.Add(tmp)

                        'Log event
                        systemLog.WriteLine(Now & " : K04, Fylkenr: " & rspOmrNr & " fetched. (" & rspNavn & ")")

                        DumpToFile(response, xmlOut, "K04-" & rspOmrNr & ".xml")
                        DumpToFile(response, queryLog & "Kommune\K04\", "K04-" & rspOmrNr & "-" & Now.ToString("yyyyMMdd_HHmmss") & ".xml")
                    End If
                End If
            End If

            'Fetch the K03 report
            If ok And kretsNr <> "" Then

                query = CreateXMLQuery("K03", , kommNr, kretsNr)
                response = DoQuery(query)

                'Try to load the XML
                Try
                    XMLDoc.LoadXml(response)
                Catch e As XmlException
                    ok = False
                    'Log error
                    systemLog.WriteLine(Now & " : Bad K03 response.")
                    LogError(e)
                End Try

                'Handle returned data
                If ok Then
                    Try
                        n = XMLDoc.SelectSingleNode("/respons/rapport/data[@navn='KommNavn']")
                        rspNavn = n.InnerText
                        n = XMLDoc.SelectSingleNode("/respons/rapport/data[@navn='KommNr']")
                        rspOmrNr = n.InnerText
                        n = XMLDoc.SelectSingleNode("/respons/rapport/data[@navn='KretsNr']")
                        rspKretsNr = n.InnerText
                    Catch e As Exception
                        ok = False

                        'Log error
                        systemLog.WriteLine(Now & " : Bad K03 response.")
                        LogError(e)
                    End Try

                    If ok Then
                        'Log event
                        systemLog.WriteLine(Now & " : K03, Kommunenr: " & rspOmrNr & "/" & rspKretsNr & " fetched. (" & rspNavn & " - " & kretsNavn & ")")

                        response = response.Replace("</rapportnavn>", "</rapportnavn>" & vbCrLf & strStatusElement)
                        DumpToFile(response, xmlOut, "K03-" & rspOmrNr & "-" & rspKretsNr & ".xml")
                        DumpToFile(response, queryLog & "Kommune\K03\", "K03-" & rspOmrNr & "-" & rspKretsNr & "-" & Now.ToString("yyyyMMdd_HHmmss") & ".xml")

                    End If

                End If

            End If

        ElseIf valg = "F" Then

            'Fetch the F02 report
            query = CreateXMLQuery("F02", , kommNr)
            response = DoQuery(query)

            'Try to load the XML
            Try
                XMLDoc.LoadXml(response)
            Catch e As XmlException
                ok = False
                'Log error
                systemLog.WriteLine(Now & " : Bad F02 response.")
                LogError(e)
            End Try

            'Store to disk
            If ok Then
                Try
                    n = XMLDoc.SelectSingleNode("/respons/rapport/data[@navn='KommNavn']")
                    rspNavn = n.InnerText
                    n = XMLDoc.SelectSingleNode("/respons/rapport/data[@navn='KommNr']")
                    rspOmrNr = n.InnerText
                Catch e As Exception
                    ok = False

                    'Log error
                    systemLog.WriteLine(Now & " : Bad F02 response.")
                    LogError(e)
                End Try

                If ok Then
                    'Log event
                    systemLog.WriteLine(Now & " : F02, Kommunenr: " & rspOmrNr & " fetched. (" & rspNavn & ")")
                    fetchedKommuner.Add("F" & kommNr)

                    response = response.Replace("</rapportnavn>", "</rapportnavn>" & vbCrLf & strStatusElement)
                    DumpToFile(response, xmlOut, "F02-" & rspOmrNr & ".xml")
                    DumpToFile(response, queryLog & "Fylke\F02\", "F02-" & rspOmrNr & "-" & Now.ToString("yyyyMMdd_HHmmss") & ".xml")
                End If
            End If

            'Fetch the F04 report
            tmp = "F" & fylkeNr
            If ok And Not fetchedFylker.Contains(tmp) Then

                query = CreateXMLQuery("F04", , fylkeNr)
                response = DoQuery(query)

                'Try to load the XML
                Try
                    XMLDoc.LoadXml(response)
                Catch e As XmlException
                    ok = False

                    'Log error
                    systemLog.WriteLine(Now & " : Bad F04 response.")
                    LogError(e)
                End Try

                'Store to disk
                If ok Then
                    Try
                        n = XMLDoc.SelectSingleNode("/respons/rapport/data[@navn='FylkeNavn']")
                        rspNavn = n.InnerText
                        n = XMLDoc.SelectSingleNode("/respons/rapport/data[@navn='FylkeNr']")
                        rspOmrNr = n.InnerText
                    Catch e As Exception
                        ok = False

                        'Log error
                        systemLog.WriteLine(Now & " : Bad F04 response.")
                        LogError(e)
                    End Try

                    If ok Then
                        fetchedFylker.Add(tmp)

                        'Log event
                        systemLog.WriteLine(Now & " : F04, Fylkenr: " & rspOmrNr & " fetched. (" & rspNavn & ")")

                        DumpToFile(response, xmlOut, "F04-" & rspOmrNr & ".xml")
                        DumpToFile(response, queryLog & "Fylke\F04\", "F04-" & rspOmrNr & "-" & Now.ToString("yyyyMMdd_HHmmss") & ".xml")
                    End If

                End If

            End If

            'Fetch the F03 report
            If ok And kretsNr <> "" Then

                query = CreateXMLQuery("F03", , kommNr, kretsNr)
                response = DoQuery(query)

                'Try to load the XML
                Try
                    XMLDoc.LoadXml(response)
                Catch e As XmlException
                    ok = False
                    'Log error
                    systemLog.WriteLine(Now & " : Bad F03 response.")
                    LogError(e)
                End Try

                'Store to disk
                If ok Then
                    Try
                        n = XMLDoc.SelectSingleNode("/respons/rapport/data[@navn='KommNavn']")
                        rspNavn = n.InnerText
                        n = XMLDoc.SelectSingleNode("/respons/rapport/data[@navn='KommNr']")
                        rspOmrNr = n.InnerText
                        n = XMLDoc.SelectSingleNode("/respons/rapport/data[@navn='KretsNr']")
                        rspKretsNr = n.InnerText
                    Catch e As Exception
                        ok = False

                        'Log error
                        systemLog.WriteLine(Now & " : Bad F03 response.")
                        LogError(e)
                    End Try

                    If ok Then
                        'Log event
                        systemLog.WriteLine(Now & " : F03, Kommunenr: " & rspOmrNr & "/" & rspKretsNr & " fetched. (" & rspNavn & " - " & kretsNavn & ")")

                        response = response.Replace("</rapportnavn>", "</rapportnavn>" & vbCrLf & strStatusElement)
                        DumpToFile(response, xmlOut, "F03-" & rspOmrNr & "-" & rspKretsNr & ".xml")
                        DumpToFile(response, queryLog & "Fylke\F03\", "F03-" & rspOmrNr & "-" & rspKretsNr & "-" & Now.ToString("yyyyMMdd_HHmmss") & ".xml")
                    End If
                End If

            End If
        End If

        'Update last-node log if we're ok
        If ok Then
            Try
                Dim w As XmlTextWriter = New XmlTextWriter(queryLog & "lastnode.xml", Encoding.GetEncoding("iso-8859-1"))
                w.Formatting = Formatting.Indented
                w.IndentChar = vbTab
                w.WriteStartDocument(True)
                node.WriteTo(w)
                w.Close()
            Catch e As Exception
                'File error
                LogError(e)
            End Try

        End If

        'Return status code
        Return ok

    End Function

    ' Sends the query to the server and returns the fetched XML
    Private Function DoQuery(ByVal queryXML As String) As String

        Dim ok As Boolean = True
        Dim msg As String
        Dim ret As String = ""

        'Create the POST data
        Dim formData As String = "XML=" & UrlEncode(queryXML)

        'Create the URI
        Dim uri As New UriBuilder(server)

        If method = "GET" Then
            uri.Query = formData
        End If

        'Create the HTTP request
        Dim request As HttpWebRequest = WebRequest.Create(uri.Uri)
        request.Method = method
        request.Timeout = timeout

        If method = "POST" Then
            request.ContentType = "application/x-www-form-urlencoded"
            request.ContentLength = Encoding.GetEncoding("iso-8859-1").GetBytes(formData).GetLength(0)

            'Write the POST data
            Dim writer As StreamWriter
            Try
                writer = New StreamWriter(request.GetRequestStream(), Encoding.GetEncoding("iso-8859-1"))
                writer.Write(formData)
                writer.Close()
            Catch e As Exception
                'Log error
                ok = False
                msg = e.Message
                systemLog.WriteLine(Now & " : Failed to write POST data.")
                LogError(e)
            End Try
        End If

        'Get the HTTP response with the resulting data
        If ok Then
            Dim response As HttpWebResponse
            Try
                response = request.GetResponse()
            Catch webEx As WebException
                'Log WEB error
                ok = False
                msg = webEx.Message
                LogError(webEx)
                systemLog.WriteLine(Now & " : HTTP request failed: " & webEx.Message)
                If Not webEx.Response Is Nothing Then webEx.Response.Close()
            End Try

            'Read the data
            If ok Then
                Dim reader As StreamReader = New StreamReader(response.GetResponseStream(), Encoding.GetEncoding("iso-8859-1"))
                ret = reader.ReadToEnd
                reader.Close()
            End If
        End If

        'Warn on error
        If Not ok And online Then

            'SMS
            If warnSMS Then
                WarnOnSMS(msg)
            End If

            'Email
            If warnEmail Then
                WarnOnEmail(queryXML, msg)
            End If

            online = False

        End If

        'Set status
        online = ok

        Return ret
    End Function

    'Dumps given XML to a specified folder/file
    Private Function DumpToFile(ByVal XML As String, ByVal path As String, ByVal filename As String) As Boolean

        'Check / create folder
        Try
            Directory.CreateDirectory(path)
        Catch e As Exception
            LogError(e)
        End Try

        'Dump the data
        Try
            Dim wrt As StreamWriter = New StreamWriter(path & filename, False, Encoding.GetEncoding("iso-8859-1"))
            wrt.WriteLine(XML)
            wrt.Close()
            wrt = Nothing
        Catch e As Exception
            'Log error
            systemLog.WriteLine(Now & " : Failed to save " & path & filename & " to disk.")
            LogError(e)
            Return False
        End Try

        Return True
    End Function

    ' Creates the query XML based on given parameters
    Function CreateXMLQuery(ByVal rapport As String, Optional ByVal index As Integer = -1, _
        Optional ByVal omrNr As String = "", Optional ByVal kretsNr As String = "", _
        Optional ByVal landsdel As String = "", Optional ByVal bydel As Integer = 0, _
        Optional ByVal fraDato As String = "", Optional ByVal fraTid As String = "", _
        Optional ByVal format As String = "XML") As String

        'Build the query
        Dim qry As String = "<?xml version='1.0' encoding='iso-8859-1'?>" & vbCrLf
        qry += "<query>" & vbCrLf
        qry += "<RapportNavn>" & rapport & "</RapportNavn>" & vbCrLf

        If index > -1 Then qry += "<LopeNr>" & index & "</LopeNr>" & vbCrLf

        If fraDato <> "" Then qry += "<FraDato>" & fraDato & "</FraDato>" & vbCrLf
        If fraTid <> "" Then qry += "<FraTid>" & fraTid & "</FraTid>" & vbCrLf

        If omrNr <> "" Then qry += "<OmrNr>" & omrNr & "</OmrNr>" & vbCrLf
        If kretsNr <> "" Then qry += "<KretsNr>" & kretsNr & "</KretsNr>" & vbCrLf
        If landsdel <> "" Then qry += "<LandsdelsNavn>" & landsdel & "</LandsdelsNavn>" & vbCrLf

        If bydel <> 0 Then qry += "<BydelsNr>" & bydel & "</BydelsNr>" & vbCrLf

        qry += "<Format>" & format & "</Format>" & vbCrLf
        qry += "</query>" & vbCrLf

        Return qry
    End Function

    'Logs an exception to error-log.txt
    Sub LogError(ByVal ex As Exception)

        Try
            errorLog = New StreamWriter(logFiles & "error-log.txt", True, Encoding.GetEncoding("iso-8859-1"))
            errorLog.WriteLine(Now & " : " & ex.Message)
            errorLog.WriteLine(ex.StackTrace & vbCrLf & vbCrLf)
            errorLog.Close()
        Catch e As Exception
        End Try

    End Sub

    'Warns on connection errors by e-Mail
    Sub WarnOnEmail(ByVal query As String, ByVal message As String)

        Dim epost As MailMessage = New MailMessage()
        SmtpMail.SmtpServer = SMTPServer

        epost.From = "505@ntb.no"
        epost.To = emailRcpt
        epost.Subject = "Mulig feil ved nedlasting av valgresultater!"
        epost.BodyFormat = Web.Mail.MailFormat.Text
        epost.Body = "Det har oppst�tt en feil i nedlastning av valgresultater. Sjekk dette!" & vbCrLf & vbCrLf
        epost.Body = epost.Body & "Tid: " & Now & vbCrLf
        epost.Body = epost.Body & "Feilmelding: " & message & vbCrLf & vbCrLf
        epost.Body = epost.Body & query

        Try
            SmtpMail.Send(epost)
        Catch e As Exception
            'Log warning error
            systemLog.WriteLine(Now & " : FAILED to send warning by e-Mail!")
            LogError(e)
        End Try

    End Sub

    'Warns on connection errors by SMS - Posts a messages to \\helsinki\out
    Sub WarnOnSMS(ByVal message As String)

        Dim nmbrs As String()
        Dim nmbr As String
        Dim txt As String

        Dim wrt As StreamWriter

        nmbrs = SMSRcpt.Split(";")

        For Each nmbr In nmbrs

            txt = "[SMS]" & vbCrLf & "From = NTB-505" & vbCrLf
            txt = txt & "To=" & nmbr & vbCrLf
            txt = txt & "Text=Det har oppst�tt er feil i nedlasting av valgresultater:�'" & message & "'�Sjekk dette!"

            Try
                wrt = New StreamWriter(SMSPath & "SM" & nmbr & ".GSM", False, Encoding.GetEncoding("iso-8859-1"))
                wrt.WriteLine(txt)
                wrt.Close()
            Catch e As Exception
                'Log warning error
                LogError(e)
                systemLog.WriteLine(Now & " : FAILED to send warning by SMS!")
                Exit For
            End Try

        Next

    End Sub

#End Region

End Class
